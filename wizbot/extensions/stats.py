from typing import List, Optional

import discord
from discord import app_commands
from discord.ext import commands
from loguru import logger

from .. import TheBot, emojis, items_db
from ..menus import ItemView

FIND_ITEM_QUERY = """
SELECT * FROM items
INNER JOIN locale_en ON locale_en.id == items.name
WHERE locale_en.data == ? COLLATE NOCASE
"""

FIND_SET_QUERY = """
SELECT * FROM set_bonuses
"""

SET_BONUS_NAME_QUERY = """
SELECT locale_en.data FROM set_bonuses
INNER JOIN locale_en ON locale_en.id == set_bonuses.name
WHERE set_bonuses.id == ?
"""


class Stats(commands.GroupCog, name="item"):
    def __init__(self, bot: TheBot):
        self.bot = bot

    async def fetch_item(self, name: str) -> List[tuple]:
        async with self.bot.db.execute(FIND_ITEM_QUERY, (name,)) as cursor:
            return await cursor.fetchall()

    async def fetch_set_bonus_name(self, set_id: int) -> Optional[tuple]:
        async with self.bot.db.execute(SET_BONUS_NAME_QUERY, (set_id,)) as cursor:
            return (await cursor.fetchone())[0]

    async def fetch_item_stats(self, item: int) -> List[str]:
        stats = []

        async with self.bot.db.execute(
            "SELECT * FROM item_stats WHERE item == ?", (item,)
        ) as cursor:
            async for row in cursor:
                a = row[3]
                b = row[4]

                match row[2]:
                    case 1:
                        stat, no_value = items_db.translate_stat(a)
                        rounded_value = int(round(items_db.unpack_stat_value(b), 2))
                        if no_value:
                            stats.append(f"{stat}")
                        else:
                            stats.append(f"+{rounded_value}{stat}")

                    case 2:
                        if a != 0:
                            stats.append(f"+{a} {emojis.PIP}")
                        if b != 0:
                            stats.append(f"+{b} {emojis.POWER_PIP}")

                    # TODO: Spells

                    case 5:
                        stats.append(f"+{a}% {emojis.SPEED}")

                    case 6:
                        stats.append(f"{a} Passenger Mount")

        return stats

    async def build_item_embed(self, row) -> discord.Embed:
        item_id = row[0]
        set_bonus = row[2]
        rarity = items_db.translate_rarity(row[3])
        jewels = row[4]
        kind = items_db.ItemKind(row[5])
        extra_flags = items_db.ExtraFlags(row[6])
        school = row[7]
        equip_level = row[8]
        min_pet_level = row[9]
        max_spells = row[10]
        max_copies = row[11]
        max_school_copies = row[12]
        deck_school = row[13]
        max_tcs = row[14]
        item_name = row[17]

        logger.info(f"Processing item {item_id}")

        requirements = []
        if equip_level != 0:
            requirements.append(f"Level {equip_level}+")
        requirements.append(items_db.translate_equip_school(school))

        stats = []
        if items_db.ExtraFlags.PET_JEWEL in extra_flags:
            stats.append(f"Level {items_db.translate_pet_level(min_pet_level)}+")
        elif kind == items_db.ItemKind.DECK:
            stats.append(f"Max spells {max_spells}")
            stats.append(f"Max copies {max_copies}")
            stats.append(
                f"Max {items_db.translate_school(deck_school)} copies {max_school_copies}"
            )
            stats.append(f"Sideboard {max_tcs}")
        stats.extend(await self.fetch_item_stats(item_id))

        embed = (
            discord.Embed(
                color=items_db.make_school_color(school),
                description="\n".join(stats) or "\u200b",
            )
            .set_author(name=item_name, icon_url=items_db.get_item_icon_url(kind))
            .add_field(name="Requirements", value="\n".join(requirements))
        )

        if jewels != 0:
            embed = embed.add_field(
                name="Sockets", value=items_db.format_sockets(jewels)
            )

        if set_bonus != 0:
            set_name = await self.fetch_set_bonus_name(set_bonus)
            embed = embed.add_field(name="Set Bonus", value=set_name)

        embed.add_field(name="Rarity", value=rarity)

        return embed

    @app_commands.command(name="find", description="Finds a Wizard101 item by name")
    @app_commands.describe(name="The name of the item to search for")
    async def find(self, interaction: discord.Interaction, *, name: str):
        logger.info("Requested item '{}'", name)

        rows = await self.fetch_item(name)
        if not rows:
            return await interaction.response.send_message(
                f"Sorry, item '{name}' is unknown to me."
            )

        view = ItemView([await self.build_item_embed(row) for row in rows])
        await view.start(interaction)


async def setup(bot: TheBot):
    await bot.add_cog(Stats(bot))
